# Netsec

## Crypto Refresher

### Language

| Term | Explanation |
| --- | --- |
| Secrecy |keep data hidden from unintended users
|Confidentiality|keep somebody else's data secret
|Privacy|keep data about a person secret, i.e. the link between data and person
|Anonymity|keep identity of protocol participant secret
|Integrity|data is unchanged
|Authentication |make sure data comes from the claimed sender
|Signature |Authentication, plus the receiver can proof authentication also to third party

### Symmetric Cryptographic Primitives

#### Stream Ciphers

- Encrypt whole message with key with the same length as message
- An example is ChaCha stream Cipher
- :x: Key must not be reused
- :x:  Alteration of ciphertext will alter corresponding values in plaintext after decryption

#### Block Ciphers

Message is split into blocks, each block is encrypted separately.
As encryption algorithm for example AES can be used.

Block ciphers describe the way of how the message and the key is feed into the
encryption algorithm, but do not describe the encryption algorithm itself.

##### Electronic Code Book (ECB)

![ECB Pic](assets/ECB.png)

- Split message in blocks, encrypt each block separately, but with same key
- :heavy_check_mark: fast
- :x: Same plaintext always corresponds to same ciphertext
- :x: Adversary may can guess part of plaintext, can decrypt parts of a message if same
ciphertext block occurs
- :x: Adversary can replace blocks with other blocks

##### Cipher Block Chaining (CBC)

![CBC Pic](assets/CBC.png)

- :heavy_check_mark: Semantic security (best guess for single plaintext bit is random)
- :x: Altered ciphertext only influences two blocks
- :x: Not secure for variable-sized messages!

##### Counter Mode (CTR)

![CTR Pic](assets/CTR.png)

- :heavy_check_mark: Semantic security
- :x: Altered ciphertext only influences single block
- :x: same vulnerabilities as stream cipher

#### Message Authentication Codes

A code attached to a message, to prove that the message was not altered ont the way to the receiver.
Can be implemented with a **hash**, **cipher** or other

#### Authenticated Encryption with Associated Data (AEAD)

A cryptographic primitive that combines **encryption** and **mac**

##### Galois Counter Mode (GCM)

![GCM Pic](assets/GCM.png)

a possible implementation of a AEAD

### Asymmetric Cryptographic Primitives

#### Diffie–Hellman

- public values: generator `g` and large prime `p`
- secret values: Alice `a`, Bob `b`
- A -> B: $`g^a (\mod p)`$
- B -> A: $`g^b (\mod p)`$
- $`g^{ab} (\mod p)`$ can now be used as key
- To solve this for `a` or `b` Eve would have to solve the discrete logarithm problem
- power calculations are easy because they are only calculated in modulo
- vulnerable to man in the middle attack, if Eve acts as Bob to Alice, and as Alice to Bob

[Explanation](http://pi.math.cornell.edu/~mec/2003-2004/cryptography/diffiehellman/diffiehellman.html)

#### RSA

### Key creation

- `p`, `q` are large secret primes
- pick `e`, compute `d` such that $`ed \equiv 1 \mod \phi(pq)`$ where $`\phi(x,y) = (x-1)*(y-1)`$
- Public key: `pq`, `e`
- Private key: `p`, `q`, `d`

### Encryption

- Signature $`\Sigma = M^d \mod pq`$

### Decryption

- $`M = \Sigma^e \mod pq = M^{de} \mod pq = M^{1+\phi(pq)} \mod pq= M \mod pq`$

#### Encrypted Key Exchange (EKE)

gitter, gatter, gotter who tis? :flushed:

### Hash Functions

#### One-Wayness

One-wayness: given output `y` (width of `n` bits), how many operations does it take to find any `x`,
such that $`H(x) = y`$?

- Assumption: best attack is random search
- For each trial `x`, probability that output is `y` is $`2^{-n}`$
- $`P[\text{find x after m trials}]=1-(1-2^{-n})^{m}`$
- Rule of thumb: find x after $`2^{n-1}`$ trials on average

#### Weak Collision Resistence

Given input x, how
many operations does it take to find another $`x’ ≠ x`$, s.t. $`H(x) = H(x’)`$?

- Assumption: best attack is random search
- For each trial `x`, probability that output is `y` is $`2^{-n}`$
- $`P[\text{find x after m trials}]=1-(1-2^{-n})^{m}`$
- Rule of thumb: find x after $`2^{n-1}`$ trials on average

#### Strong Collision Resistence

How many operations does it take to find a `x` and `x'` s.t. $`x’ ≠ x`$ and $`H(x) = H(x’)`$?

- Assumption: best attack is random search
- Algorithm picks random `x’`, checks whether `H(x’)` matches any other output value previously seen
- $`P[\text{find col after m trials}]= 1-(1-1/2^n )(1-2/2^n )(1-3/2^n )...(1-(m+1)/2^n )`$
- Rule of thumb: find x after $`2^{n/2}`$ trials on average

### One-Way Hash Chains

Chain hash functions together. Improves One-Wayness (glaub :flushed:)

### Merkle Hash Trees

![Merkel](assets/Merkel.png)

Use leafs to calculate parent. Verify leaf easily

## PKI

### PKI Extensions

#### HTTP Public Key Pinning (HPKP)

Enables a server to specify certain set of certificates that are valid for a certain time. The client saves this set and only accepts these certificates during the specified time.

#### HTTP Strict Transport Security (HSTS)

The server communicates to the client, that it will only accept secure (HTTPS) connections for a certain time for this domain.

#### Online Certificate Status Protocol (OCSP)

A protocol used to check if a certificate is still valid.

#### DNS-Based Authentication of Named Entities (DANE)

- Goal: authenticate TLS servers without a certificate authority (CA)
- Idea: use DNSSEC to bind certificates to names

#### Certificate Transparency (CT)

A concept, where all issued certificates are saved in a cental publicly viewable merkle hash tree, such that they can easily be verified by an user.

* **SCT**: Because the log server can only periodically update its log, a valid certificate
  submitted by the CA cannot be immediately added to the log. The log server creates SCT
as a promise to add the certificate to the log within some time period.
The domain owner can use SCT to show the validity of the certificate to the user before the certificate
is added to the log.

## TLS

### 2-Round Handshake

```mermaid
sequenceDiagram
    participant Client
    participant Server
    rect rgba(255, 0, 0, .1)
    Client->>Server: Client Hello, supported CipherSuites
    end

    rect rgba(0, 0, 255, .1)
    Server->>Client: Server Hello
    Server->>Client: Certificate incl. Public Key Ps
    Server->>Client: Chosen CipherSuite
    Server->>Client: ServerKeyExchange
    Server->>Client: Server Hello Done
    end

    Note left of Client: Checks Certificate
    Note left of Client: Generates random <br/>"pre-master secret"
    Note left of Client: Generates "master secret" <br/>from "pre-master secret"

    rect rgba(255, 0, 0, .1)
    Server->>Client: ClientKeyExchange
    Client->>Server: pre-master secret encrypted with Ps
    Client->>Server: Change Cipher Spec (to symmetric)
    Client->>Server: Finished
    end
    Note right of Server: Generates "master secret" <br/>from "pre-master secret"

    rect rgba(0, 0, 255, .1)
    Server->>Client: Change Cipher Spec (to symmetric)
    Server->>Client: Finished
    end
    rect rgba(255, 0, 0, .1)
    Client->>Server: Symmetrically Encrypted Data
    end
```

### 1-Round Handshake

```mermaid
sequenceDiagram
    participant Client
    participant Server
    rect rgba(255, 0, 0, .1)
    Client->>Server: Client Hello
    Client->>Server: proposed CipherSuite
    Server->>Client: ClientKeyExchange
    end
    Note right of Server: Generates random <br/>"pre-master secret"
    Note right of Server: Generates "master secret" <br/>from "pre-master secret"
    rect rgba(0, 0, 255, .1)
    Server->>Client: Server Hello
    Server->>Client: ServerKeyExchange
    Server->>Client: Chosen CipherSuite
    Server->>Client: Finished + Hash of Handshake
    rect rgba(255, 0, 0, .1)
    Server->>Client: Encrypted Application Data
    end
    end
    Note left of Client: Generates "master secret" <br/>from "pre-master secret"

    rect rgba(255, 0, 0, .1)
    Client->>Server: Finished + Hash of Handshake
    rect rgba(0, 0, 255, .1)
    Client->>Server: Encrypted Application Data
    end
    end
```

### 0-Round Handshake
0-RTT data is not replay protected, and therefore should only be used for idempotent request.

### Perfect forward secrecy
Session keys will not be compromised, even if long-term secrets used in the session key exchange are compromised.

### Cipher suites
A cipher suite has the following format:
`TLS_KeyExchange_Authentication_WITH_Encryption_MAC`
For example:
`TLS_DHE_RSA_WITH_AES_128_GCM_SHA256`

- **KeyExchange**
  - `DH`: Fixed Diffie-Hellman
    *No perfect forward secrecy*
  - `ECDHE`: Ephemeral Elliptic Curve Diffie-Hellman
    *provides perfect forward secrecy*
  - `DHE`: Ephemeral Diffie-Hellman
    *provides perfect forward secrecy*
  - `DH_anon`: Diffie-Hellman, no Authentication
    *No authentication, vulnerable to MitM*
- **Authentication**:
  - `RSA`: Public-Key Cryptography
  - `anon`: No
- **Encryption**:
  - `AES_256_CBC` 128-bit AES CBC mode
  *CBC is vulnerable to padding oracle attacks*
  - `AES_128_GCM` 128-bit AES GCM mode
  *Secure*
  - `DES_CBC` DES in CBC mode
  *DES is not secure*
  - `RC4_128` 128-bit RC4
  *RC4 is not secure*
- **MAC**
  - `SHA`: SHA-1
  *Not collision resistant, but no significant security concerns.*
  - `SHA256`: 256-bit SHA-2
  *Secure*
  - `MD5`
  *Broken*

## BGP

### Route Origin Authentication (ROA)

#### Terminology:

- **RIR:** Regional Internet Registry
- **RPKI:** Resource Public Key Infrastructure

#### Scheme

```mermaid
sequenceDiagram
    participant RIR
    participant AS X
    participant Regional RPIKE
    participant AS Y
    RIR ->> AS X: Issue certficicate for IP <br> prefixes and AS number
    Note right of AS X: Create ROAs signed <br> with certficicate
    AS X->> Regional RPIKE: Publish ROA
    AS X ->> AS Y: Announce prefix
    AS Y ->> Regional RPIKE: Query prefix
    Regional RPIKE ->> AS Y: ROA
    Note right of AS Y: Check if ROA is <br>valid for prefix, <br> accept announce-<br>ment if so
```

## Anonymous-Communication Systems

### Mix-nets
- Collect several messaging before forwarding to avoid fingerprinting
- Attacks:
  - Intersection Attack

    ![Intersection Attack](assets/intersection_attack.png)

  - Statistical disclosure

## Firewalls, Intrusion Detection & Evasion

### Common Terms

- **FW:** Firewall
- **IDS:** An Intrusion Detection System (IDS) is a device or application that analyzes
  whole packets. The IDS operates passively and does not block attacks.
- **IPS:** An Intrusion Protection System (IPS) is a device or application that
  analyzes whole packets. It  can block attacks.
- **NGFW**: A Next-Generation Firewall (NGFW). It acts as both a traditional Firewall and IPS,
- **WAF**: A Web Application Firewall (WAF) filters, monitors, and blocks HTTP traffic to and from a web
  application.

### Malware

#### Mirai Attack

The Mirai attack has the following phases:

1. Scan: Look for open Telnet ports. It does so by sending TCP-SYN packets to
   randomly generated IPv4 addresses. The generation algorithm strategically
   excludes some addresses, like the ones assigned to the USA
   Department of Defense.

2. Brute force: The bot tries to log in using 10 random user/pass
   combinations taken from a hard coded list of 62. These represent
   default credentials of existing devices.

3. Report: upon login success, the bot reports the target to a Report
   Server. It lists IP address, type of device (if available) and
   successful credentials.

4. Infection: a Loader program gets a detail of the target from the
   Report Server, connects to Telnet and uploads the correct malware
   binary to the machine.

#### Detection Evasion by Design

##### Polymorphism

- Swapping of equivalent code constructs
- Changing the order of code
- Inserting noise
- Compiler modulation


## DNS

### DNSsec

#### Records

![DNS Records](assets/dns_records.png)

### DNSsec vs. DOH
|Property| DNSSEC | DOH |
|--------|--------|-----|
| Availability | high availability | more complex, may fail due to certificate errors |
| Performance | offline crypto --> fast | multiple roundtrips, online asymmetric crypto --> slow |
| Backwards compatibility | yes | no |
| Roots of trust | less | more --> more vulnerable |

## SCION

### Dynamically Recreatable Key (DRKey)

Process for an AS **C** to authenticate a SCMP message sent by Host **H** in AS **A** wit DRKey:
![DRKey authentication](assets/verify-DRKey.png)


## Probabilistic Traffic Monitoring

### Overview

#### General Purpose

| Name            | Functioning                  | Advantage                                             | Disadvantage                        |
| ---             | ---                          | ---                                                   | ---                                 |
| Sampled NetFlow | Sample every $`k`$-th packet | • Simple to implement <br/> • Reduced processing time | • Memory Overhead <br/> • imprecise |

#### Find large Flows

| Name              | Functioning                                                                                | Advantage                                               | Disadvantage |
| ---               | ---                                                                                        | ---                                                     | ---          |
| Sample and Hold   | • Sample byte with probability <br/> • update flow entry for all following packets         | No over-counting                                        |              |
| Multistage Filter | <img src="assets/multistage_filter.png" width=400>                                         | • no FNs <br/> • low FPs <br/> • fixed memory resources |              |
| MG-Algorithm      | <img src="assets/frequent_item_finding.png" width=400> <br/> • Second pass to eliminate FP |
| EARDet 	    | Modify MG: Count packet size <br/> <img src="assets/eardet.png" width=400> | • No FN for large flows <br/> • No FP for small flows | per-packet counter update is expensive |

#### Finding Duplicates

| Name               | Functioning                  | Advantage                                        | Disadvantage |
| ---                | ---                          | ---                                              | ---          |
| Bloom Filter <br/> | Set bits at hash values to 1 | • no FN <br/> • constant time insertion and test | • O(n) space |

#### Estimate number of flows

| Name                                | Functioning                                          | Advantage         | Disadvantage                                                                              |
| ---                                 | ---                                                  | ---               | ---                                                                                       |
| Probabilistic Counting              | • Hash flow into \[0,1\) <br/> • keep smallest value | easy to implement | • Attacker controlling 1 input can bias <br/> • minimum has large variance → not robust |
| Keep track of $`k`$ smallest values |                                                      | • variance is smaller <br/> • harder to influence | |


### Sample and Hold

1. For every packet, check if its flow record exists
   - If yes, hold the packet
   - If no, sample with probability $`p`$
2. Update flow entries associated with sampled & held packets
3. Flows in the cache = identified large flows

![Sample and Hold](assets/sample_and_hold.png)


## Current Internet State
- Stone Age
- Wild West
- no-brainer
